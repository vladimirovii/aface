<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hotel".
 *
 * @property integer $idhotel
 * @property string $name
 * @property string $country
 */
class Hotel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country'], 'required'],
            [['name'], 'string', 'max' => 145],
            [['country'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idhotel' => 'Idhotel',
            'name' => 'Name',
            'country' => 'Country',
        ];
    }
}

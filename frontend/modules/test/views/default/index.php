<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<h1>Country</h1>
<?php
    $form = ActiveForm::begin();
    foreach($country as $key => $value)
    {
        $data[$value['country']] = $value['country'] . " - " . $value['count(*)'];
    }
?>
<?= $form->field($model, 'name')->dropDownList($data, ['prompt'=>'Select...']) ?>
<?=Html::submitButton('Find', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end(); ?>

<h1>Hotels</h1>
    <?php    foreach($models as $row):        ?>
        <div>
            <img src="http://infolith.bget.ru/frontend/web/images/blog/1.jpg" width="200" height="150" alt="">
            <h3>Name: <?=$row['name']?></h3>
            <p>Location: <?=$row['country']?></p>
            <hr>
        </div>
    <?php    endforeach;    ?>
<?php
   echo \yii\widgets\LinkPager::widget(['pagination' => $pages]);
?>
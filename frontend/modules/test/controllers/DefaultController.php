<?php
namespace app\modules\test\controllers;

use common\models\Country;
use common\models\Hotel;
use yii\data\Pagination;
use yii\web\Controller;
use yii\db\Query;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $model = new Country();

        $connection = \Yii::$app->db;
        $country = $connection->createCommand('SELECT country, count(*) FROM hotel GROUP BY country ORDER BY count(*) DESC')->queryAll();

        if(\Yii::$app->request->post("Country")){
            $query = Hotel::find()->where(['country' => \Yii::$app->request->post("Country")]);
        }else{
            $query = Hotel::find();
        }

        $pages = new Pagination([
            'totalCount' => $query->count(),
            'defaultPageSize' => 10,
        ]);
        $models = $query->offset($pages->offset)
                     ->limit($pages->limit)
                     ->all();

        return $this->render('index', [
            'country' => $country,
            'model' => $model,
            'models' => $models,
            'pages' => $pages,
        ]);
    }
}
